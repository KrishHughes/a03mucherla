QUnit.test("Here's a test that should always pass", function (assert) {
    assert.ok(1 <= "3", "1<3 - the first agrument is 'truthy', so we pass!");
});

QUnit.test('Testing calculateArea function with several sets of inputs', function (assert) {
    assert.equal(calculation(1, 1,1), 2.3, 'Tested with three relatively small positive numbers');
    assert.equal(calculation(50, 20,25),1725, 'Tested with three relatively large positive numbers');
    assert.equal(calculation(50.459, 20.673,25.89),1815.11730855, 'Tested with three relatively large floating point positive numbers');
});

